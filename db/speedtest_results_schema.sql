-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 31, 2022 at 08:59 PM
-- Server version: 10.6.9-MariaDB-log
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

--
-- Table structure for table `speedtest_results`
--

CREATE TABLE `speedtest_results` (
  `id` int(11) NOT NULL,
  `epoch` int(11) NOT NULL,
  `timestamp` text NOT NULL,
  `ping_jitter` float NOT NULL,
  `ping_latency` float NOT NULL,
  `download_bandwidth` int(11) NOT NULL,
  `download_bytes` int(11) NOT NULL,
  `download_elapsed` int(11) NOT NULL,
  `upload_bandwidth` int(11) NOT NULL,
  `upload_bytes` int(11) NOT NULL,
  `upload_elapsed` int(11) NOT NULL,
  `packet_loss` int(11) NOT NULL,
  `isp` text NOT NULL,
  `interface_internal_ip` text NOT NULL,
  `interface_name` text NOT NULL,
  `interface_mac_addr` text NOT NULL,
  `interface_is_vpn` tinyint(1) NOT NULL,
  `interface_external_ip` text NOT NULL,
  `server_id` int(11) NOT NULL,
  `server_host` text NOT NULL,
  `server_port` int(11) NOT NULL,
  `server_name` text NOT NULL,
  `server_location` text NOT NULL,
  `server_country` text NOT NULL,
  `server_ip` text NOT NULL,
  `result_id` text NOT NULL,
  `result_url` text NOT NULL,
  `result_persisted` int(11) NOT NULL,
  `download_server_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `speedtest_results`
--
ALTER TABLE `speedtest_results`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `speedtest_results`
--
ALTER TABLE `speedtest_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
