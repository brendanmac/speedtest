import mysql.connector
import os
import sys
import subprocess
import time
import json

DEBUG=0

# Output File Configuration (as backup, in case database is not available)
results_file = "/home/brendan/speedtest_json_results.json"

# Database Configuration Globals
myDbHost="localhost",
myDatabase="redacted",
myDbUser="redacted",
myDbPassword="redacted"

# Tunnel Configuration Globals
mySshPort       = 22; 
mySshLocalPort  = 3306;
mySshFromHost   = 'localhost'
mySshRemotePort = 3306
mySshUsername   = 'redacted'
mySshHost       = 'redacted'

# This function will get the current time in Linux epoch, seconds since Jan 1, 1970.
# This is easier than converting the timestamp output from the speedtest application.
#  Also, the timestamp output from the speedtest application varied between csv and json output.
#
def get_time():

  # Get the current time in Linux time - format required by database.
  epoch_time = int(time.time())

  if DEBUG == 1:
    print("Current time: ", epoch_time)

  return epoch_time

# This function will call a system call using subprocess.
# The process executed shall be the Ookla speedtest application
# @return The json putput from the speedtest application
#
def run_speedtest():

  # Run the speed test
  #  speedtest -p no -f json -u bps
  #   -p no = Progress output supressed
  #   -f json = json output format
  #   -u bps =  bits per second which seems to actually be Bytes (the default, but being extra clear here)
  proc = subprocess.Popen(['speedtest', '-p', 'no', '-f', 'json', '-u', 'bps'], stdout=subprocess.PIPE)
  (out, err) = proc.communicate()
  
  # TODO: Add check for error
  if DEBUG == 1:
    print("run_speedtest process output: ", out)
  
  # Back up the data into file, should database connection fail
  append_to_file(results_file, out)

  return out

# Converts binary JSON to a python dictionary for easy lookup.
#
# @param[in] speedtest_json - The binary JSON string containing the output from the Ookla speedtest
# @return A dictionary containing the provided JSON
#
def convert_json_to_dict(speedtest_json):

  if DEBUG == 1:
    print("convert_json_to_dict - speedtest_json: " + speedtest_json.decode())

  # Decode to change from binary to string
  speed_dict = json.loads(speedtest_json.decode())

  return speed_dict

# This function will append the given binary data to the given file.
# Append will create the file if it does not exist.
#
# @param[in] myFile - The path to the file the data is to be written.
# @param[in] data   - The binary data to be written to file
# 
def append_to_file(myFile, data):
  results_file = open(myFile, "ab")  # append mode, binary
  results_file.write(data)
  results_file.close()

# Connect to the database using msql.connector.
#
# Note that this requires the database to be available.
# If hosted on a provider running cpanel, this may not be directly
#  available without a tunnel,
#
# @return mysql.connector database, or exit program on error
#
def connect_to_database():

  if DEBUG == 1: 
    print("connect_to_database: Attempting to connect to database")

  # Open connection to the database.
  #
  # This requires the ssh tunnel to be open.
  #  ssh -N -f -p [remote server port running ssh] -L 3306:localhost:3306 user@domain
  #
  # brendan@localhost:~/speedtest.app$ netstat -tulpn | grep 3306
  # (Not all processes could be identified, non-owned process info
  #  will not be shown, you would have to be root to see it all.)
  # tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      2594/ssh        
  # tcp6       0      0 ::1:3306                :::*                    LISTEN      2594/ssh  
  try:
    mydb = mysql.connector.connect(
      host=myDbHost,
      database=myDatabase,
      user=myDbUser,
      password=myDbPassword
    )
  except mysql.connector.Error as err:
    sys.exit("connect_to_data_base - ERROR: {}".format(err))

  if DEBUG == 1:
    print("connect_to_database - database: ", mydb)

  return mydb

# Close the connection to the given database
# @param[input] mydb - The msql.connector database
#
def disconnect_database(mydb):

  if DEBUG == 1:
    print("disconnect_database - Disconnecting from database: ", mydb)

  mydb.close()

# The database schema is designed to match the output from the Ookla speedtest json output.
#
# @param[input] mydb        - The msql.connector database
# @param[input] epoch _time - The time this program was started (seconds since Linux epoch)
# @param[input] json_rec    - The speedtest results in json converted to dictionary
#
def insert_record_into_database(mydb, epoch_time, json_rec):

  epoch = epoch_time
  timestamp = json_rec['timestamp']

  ping_jitter = json_rec['ping']['jitter']
  ping_latency = json_rec['ping']['latency'] 

  download_bandwidth = json_rec['download']['bandwidth'] # Bytes per second
  download_bytes = json_rec['download']['bytes']
  download_elapsed = json_rec['download']['elapsed']

  upload_bandwidth = json_rec['upload']['bandwidth'] # Bytes per second
  upload_bytes = json_rec['upload']['bytes']
  upload_elapsed = json_rec['upload']['elapsed']

  packet_loss = 0
  if json_rec.get("packetLoss") is not None:
    packet_loss = json_rec['packetLoss']
  else:
    print("'packetLoss' not in json, defaulting to 0.")

  isp = "Unknown"
  if json_rec.get("isp") is not None:
    isp = json_rec['isp']
  else:
    print("'isp' not in json, defaulting to 'Unknown'.")

  interface_internal_ip = json_rec['interface']['internalIp']
  interface_name = json_rec['interface']['name']
  interface_mac_addr = json_rec['interface']['macAddr']
  interface_is_vpn = json_rec['interface']['isVpn']
  interface_external_ip = json_rec['interface']['externalIp']

  server_id = json_rec['server']['id'] 
  server_host = json_rec['server']['host']
  server_port = json_rec['server']['port']
  server_name = json_rec['server']['name']
  server_location = json_rec['server']['location']
  server_country = json_rec['server']['country']
  server_ip = json_rec['server']['ip']

  result_id = json_rec['result']['id']
  result_url = json_rec['result']['url']
  result_persisted = json_rec['result']['persisted']

  download_server_count = 1

  if DEBUG==1:
    print("Epoch:                 ", epoch)
    print("Timestamp:             ", timestamp)
    print("Jitter:                ", ping_jitter)
    print("Latency:               ", ping_latency)
    print("Download Bps:          ", download_bandwidth) 
    print("Download Bytes:        ", download_bytes)
    print("Download Elapsed:      ", download_elapsed)
    print("Upload Bps:            ", upload_bandwidth)
    print("Upload Bytes:          ", upload_bytes)
    print("Upload Elapsed:        ", upload_elapsed)
    print("Packet Loss:           ", packet_loss)
    print("ISP:                   ", isp)
    print("Interface Internal IP: ", interface_internal_ip)
    print("Interface Name:        ", interface_name)
    print("Interface MAC Addr:    ", interface_mac_addr)
    print("Interface Is VPN?      ", interface_is_vpn)
    print("Interface External IP: ", interface_external_ip)
    print("Server ID:             ", server_id)
    print("Server Host:           ", server_host)
    print("Server Port:           ", server_port)
    print("Server Name:           ", server_name)
    print("Server Location:       ", server_location)
    print("Server Country:        ", server_country)
    print("Server IP:             ", server_ip)
    print("Result ID:             ", result_id)
    print("Result URL:            ", result_url)
    print("Download Server Count: ", download_server_count)

  mycursor = mydb.cursor()

  sql = "INSERT INTO speedtest_results " \
          "(epoch, timestamp, ping_jitter, ping_latency, download_bandwidth, download_bytes, download_elapsed, " \
          "upload_bandwidth, upload_bytes, upload_elapsed, packet_loss, isp, interface_internal_ip, interface_name, " \
          "interface_mac_addr, interface_is_vpn, interface_external_ip, server_id, server_host, server_port, " \
          "server_name, server_location, server_country, server_ip, result_id, result_url, result_persisted, " \
          "download_server_count) " \
        "VALUES "\
          "(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
  
  val = (epoch, timestamp, ping_jitter, ping_latency, download_bandwidth, download_bytes, download_elapsed, \
         upload_bandwidth, upload_bytes, upload_elapsed, packet_loss, isp, interface_internal_ip, interface_name, \
         interface_mac_addr, interface_is_vpn, interface_external_ip, server_id, server_host, server_port, \
         server_name, server_location, server_country, server_ip, result_id, result_url, result_persisted, \
         download_server_count)

  mycursor.execute(sql, val)

  mydb.commit()

  if DEBUG == 1:
    print(mycursor.rowcount, "record inserted.")

  return

# Stand up the tunnel to the MySQL Server
#
# The will start an ssh session in the background forwarding the local port to the remote port.
# This allows the MySQL server to be on a remote server. Many ISPs do not allow direct access
# to the MySQL instance for security - otherwise port 3306 would be constantly attacked.
#
# This will ssh on the given port and tunnel the traffic from the local port to the remote
# port over the ssh connection.
# 
# @param[in] ssh_port - The port sshd is running on remote server
# @param[in] local_port - The local port to forward
# @param[in] from_host - The host of the local_port
# @param[in] remote_port - The remote port to forward to
# @param[in[ username - The username to use to authenticate
# @param[in] host - The host to log into. Makes login id: [username]@[host]
#
def start_ssh_tunnel(ssh_port, local_port, from_host, remote_port, username, host):

  cmd = "ssh -N -f -p " + str(ssh_port) + " -L " + str(local_port) + ":" + from_host + ":" + str(remote_port) + " " + username + "@" + host
  
  if DEBUG == 1:
    print("start_ssh_tunnel: ", cmd)

  proc = None
  
  try:
    proc = subprocess.Popen(cmd.split(), shell=False, stdout=subprocess.PIPE)
    if DEBUG == 1:
      print("   Started pid - ", proc.pid)
  except subprocess.CalledProcessError as e:
    print(e.output)

  return proc

# Break down the tunnel to the MySQL Server
#
# This will stop the ssh tunnel started by start_ssh_tunnel() by using pkill
# and the exact command used to start it. This should be called before connecting
# to the database rather than leaving the tunnel always open. It has been observed 
# that while ssh is running, the tunnel is failed. Therefore, stop and restart, 
# and then stop again before program exit.
# 
# @param[in] ssh_port - The port sshd is running on remote server
# @param[in] local_port - The local port to forward
# @param[in] from_host - The host of the local_port
# @param[in] remote_port - The remote port to forward to
# @param[in[ username - The username to use to authenticate
# @param[in] host - The host to log into. Makes login id: [username]@[host]
#
def stop_ssh_tunnel(ssh_port, local_port, from_host, remote_port, username, host):

  cmd = "ssh -N -f -p " + str(ssh_port) + " -L " + str(local_port) + ":" + from_host + ":" + str(remote_port) + " " + username + "@" + host

  if DEBUG == 1:
    print("stop_ssh_tunnel: Stopping - '" + cmd + "'")

  proc = subprocess.Popen('/usr/bin/pkill -f \"' + cmd + '\"', shell=True, stdout=subprocess.PIPE)

# The main function of this application.
#
# 1. Get the current system time as Linux epoch
# 2. Run the speed test, store result in variable and output to file
# 3. Kill ssh tunnel, if running (sometimes the connection is stale)
# 4. Start ssh tunnel
# 5. Connect to the database
# 6. Write the data to the database
# 7. Disconnect from the database.
# 8. Kill ssh tunnel
#
def main():

  # Get the current system time as Linux epoch
  epoch_time = get_time()

  # Run the speed test, store result in variable and output to file
  # Sample binary json data to simulate speedtest result.
  #speedtest_json = b'{"type":"result","timestamp":"2022-08-28T03:32:22Z","ping":{"jitter":0.63600000000000001,"latency":3.2309999999999999},"download":{"bandwidth":74815193,"bytes":657503496,"elapsed":8714},"upload":{"bandwidth":111147706,"bytes":1119899950,"elapsed":10412},"packetLoss":0,"isp":"Verizon Fios","interface":{"internalIp":"192.168.1.215","name":"eth0","macAddr":"00:00:00:00:00:00","isVpn":false,"externalIp":"10.10.10.1"},"server":{"id":21542,"host":"va.na.speedtest.i3d.net","port":8080,"name":"i3D.net","location":"Ashburn, VA","country":"United States","ip":"104.153.87.213"},"result":{"id":"FAKE-d29f-4ac0-ab92-11b1111d1b11","url":"https://www.speedtest.net/result/c/FAKE-d29f-4ac0-ab92-11b1111d1b11","persisted":true}}'
  speedtest_json = run_speedtest()

  # Convert to dictionary
  speedtest_dict = convert_json_to_dict(speedtest_json)

  # Stand up SSH tunnel to MySQL Server
  ssh_port    = mySshPort 
  local_port  = mySshLocalPort
  from_host   = mySshFromHost
  remote_port = mySshRemotePort
  username    = mySshUsername
  host        = mySshHost

  stop_ssh_tunnel(ssh_port, local_port, from_host, remote_port, username, host)
  time.sleep(1) 

  pid = start_ssh_tunnel(ssh_port, local_port, from_host, remote_port, username, host)
  time.sleep(1)

  # Connect to database, write data, disconnect from database
  mydb = connect_to_database()
  insert_record_into_database(mydb, epoch_time, speedtest_dict)
  disconnect_database(mydb)
  time.sleep(1)

  stop_ssh_tunnel(ssh_port, local_port, from_host, remote_port, username, host)

main()
